<h1>Add Movies</h1>
<?php if($locals['success'] === TRUE) { ?>
  <p>Add to Database Successfully!</p>
<?php } ?>

<form action='' method='post'>

    <label for="CinemaID">Cinema ID</label>
    <input type="number" id='Cinemaid' name='Cinema_id'>
    <?= $locals ['message']['EmptyCinemaID'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['ErrorID'] ?>
    <br/><br/>
    <label for="MovieName">Movie Name</label>
    <input type="text" id='Movie_Name' name='Movie_Name'>
    <?= $locals ['message']['MovieName'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['nameError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieName'] ?>
    <br/><br/>
    <label for="MovieLength">Movie Length</label>
    <input type="text" id='Movie_Length' name='Movie_Length'>
    <?= $locals ['message']['MovieLength'] ?>&nbsp;&nbsp;&nbsp;<?= $locals ['message']['ErrorLength'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieLength'] ?>
    <br/><br/>
    <label for="MovieDescription">Movie Description</label>
    <input type="text" id='Movie_Description' name='Movie_Description'>
    <?= $locals ['message']['MovieDescription'] ?>&nbsp;&nbsp;&nbsp;<?= $locals ['message']['DescriptionError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieDescription'] ?>
    <br><br>
    <label for="MovieDate">Movie Date</label>
    <input type="datetime-local" id='Movie_Date' name='Movie_Date'><?= $locals ['message']['MovieDate'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>
<a href='<?= APP_BASE_PATH ?>/movie_menu'>Go Back</a>