<h1>Search Cinema</h1>
<?php if($locals['success'] === TRUE) { ?>   
<?php } ?>

<form action='<?= APP_BASE_PATH ?>/search_cinema' method='post'>

    <label for="CinemaID">Cinema ID</label>
    <input type="number" id='Cinemaid' name='Cinema_id'><?= $locals ['message']['EmptyID'] ?>&nbsp;&nbsp;&nbsp; <?= $locals ['message']['ErrorID'] ?>&nbsp;&nbsp;&nbsp;<?= $locals ['sanitize_msg']['NotID'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>

<?php foreach($locals['cinemaResult'] as $cinema) { ?>
        <p>Cinema ID: <?= $cinema['CinemaID'] ?></p>
        <p>Cinema Name: <?= $cinema['CinemaName'] ?></p>
        <p>Cinema Address: <?= $cinema['CinemaAddress'] ?></p>
        <p>LandLine: <?= $cinema['LandLine'] ?></p>
        <p>Open Time: <?= $cinema['OpenTime'] ?></p>
        <p>Closed Time: <?= $cinema['ClosedTime'] ?></p>
        <br>
<?php } ?>

<li><a href='<?= APP_BASE_PATH ?>/cinema_menu'>Go Back</a></li>