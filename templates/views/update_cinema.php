<h1>Update Cinema</h1>
<?php if($locals['success'] === TRUE) { ?>
  <p>Update Cinema Successfully!</p>
<?php } ?>

<a href='<?= APP_BASE_PATH ?>/cinema_menu'>Go Back</a>
<form action='<?= APP_BASE_PATH ?>/update_cinema' method='post'>

    <input type="hidden" id='Cinemaid' name='Cinema_id' value='<?= $locals['updateid'] ?>'>
    <br/>
    <label for="CinemaName">Cinema Name</label>
    <input type="text" id='cinema_name' name='Cinema_Name' value='<?= $locals['cinemaParameter']['CinemaName'] ?>'>
    <?= $locals ['message']['EmptyName'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['nameError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotName'] ?>
    <br/><br/>
    <label for="CinemaAddress">Cinema Address</label>
    <input type="text" id='cinema_address' name='Cinema_Address' value='<?= $locals['cinemaParameter']['CinemaAddress'] ?>'>
    <?= $locals ['message']['CinemaAddress'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['addressError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotAddress'] ?>
    <br/><br/>
    <label for="CinemaLandLine">LandLine</label>
    <input type="text" id='land_line' name='Land_Line' value='<?= $locals['cinemaParameter']['LandLine'] ?>'>
    <?= $locals ['message']['LandLine'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['landlineError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotLandline'] ?>
    <br/><br/>
    <label for="CinemaOpenTime">Open Time</label>
    <input type="time" id='open_time' name='Open_Time' value='<?= $locals['cinemaParameter']['OpenTime'] ?>'>
    <?= $locals ['message']['OpenTime'] ?>
    <br><br>
    <label for="CinemaClosedTime">Closed Time</label>
    <input type="time" id='closed_time' name='Closed_Time' value='<?= $locals['cinemaParameter']['ClosedTime'] ?>'>
    <?= $locals ['message']['ClosedTime'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>