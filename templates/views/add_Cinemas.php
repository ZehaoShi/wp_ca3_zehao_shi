<h1>Add Cinema</h1>
<?php if($locals['success'] === TRUE) { ?>
  <p>Add to Database Successfully!</p>
<?php } ?>

<form action='' method='post'>

    <label for="CinemaName">Cinema Name</label>
    <input type="text" id='cinema_name' name='Cinema_Name'>
    <?= $locals ['message']['EmptyName'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['nameError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotName'] ?>
    <br/><br/>
    <label for="CinemaAddress">Cinema Address</label>
    <input type="text" id='cinema_address' name='Cinema_Address'>
    <?= $locals ['message']['CinemaAddress'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['addressError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotAddress'] ?>
    <br/><br/>
    <label for="CinemaLandLine">LandLine</label>
    <input type="text" id='land_line' name='Land_Line'>
    <?= $locals ['message']['LandLine'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['landlineError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['NotLandline'] ?>
    <br><br>
    <label for="CinemaOpenTime">Open Time</label>
    <input type="time" id='open_time' name='Open_Time'>
    <?= $locals ['message']['OpenTime'] ?>
    <br><br>
    <label for="CinemaClosedTime">Closed Time</label>
    <input type="time" id='closed_time' name='Closed_Time'>
    <?= $locals ['message']['ClosedTime'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>
<a href='<?= APP_BASE_PATH ?>/cinema_menu'>Go Back</a>