<h1>Search Movies</h1>
<?php if($locals['success'] === TRUE) { ?>   
<?php } ?>

<form action='<?= APP_BASE_PATH ?>/search_movie' method='post'>

    <label for="MovieID">Movie ID</label>
    <input type="number" id='Movie_ID' name='Movie_ID'> <?= $locals ['message']['EmptyID'] ?>&nbsp;&nbsp;&nbsp; <?= $locals ['message']['ErrorID'] ?>&nbsp;&nbsp;&nbsp;<?= $locals ['sanitize_msg']['NotID'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>

<?php foreach($locals['MovieResult'] as $movie) { ?>
        <p>Movie ID: <?= $movie['MovieID'] ?></p>
        <p>Cinema ID: <?= $movie['CinemaID'] ?></p>
        <p>Movie Name: <?= $movie['MovieName'] ?></p>
        <p>Movie Length: <?= $movie['MovieLength'] ?></p>
        <p>Movie Description: <?= $movie['MovieDescription'] ?></p>
        <p>Movie Date: <?= $movie['MovieDate'] ?></p>
        <br>
<?php } ?>

<a href='<?= APP_BASE_PATH ?>/movie_menu'>Go Back</a>