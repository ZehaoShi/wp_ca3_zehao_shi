<h1>Update Movies</h1>
<?php if($locals['success'] === TRUE) { ?>
  <p>Update Database Successfully!</p>
<?php }else?>
<a href='<?= APP_BASE_PATH ?>/movie_menu'>Go Back</a>
<form action='' method='post'>

    <input type="hidden" id='Movie_ID' name='Movie_ID' value='<?= $locals['updateid'] ?>'>
    <br/>
    <input type="hidden" id='Cinemaid' name='Cinema_id' value='<?= $locals['updatecinemaid'] ?>'>
    <label for="MovieName">Movie Name</label>
    <input type="text" id='Movie_Name' name='Movie_Name' value='<?= $locals['movieParameter']['MovieName'] ?>'>
    <?= $locals ['message']['MovieName'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['nameError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieName'] ?>
    <br/><br/>
    <label for="MovieLength">Movie Length</label>
    <input type="text" id='Movie_Length' name='Movie_Length' value='<?= $locals['movieParameter']['MovieLength'] ?>'>
    <?= $locals ['message']['MovieLength'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['ErrorLength'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieLength'] ?>
    <br/><br/>
    <label for="MovieDescription">Movie Description</label>
    <input type="text" id='Movie_Description' name='Movie_Description' value='<?= $locals['movieParameter']['MovieDescription'] ?>'>
    <?= $locals ['message']['MovieDescription'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['message']['DescriptionError'] ?>&nbsp;&nbsp;&nbsp;
    <?= $locals ['sanitize_msg']['MovieDescription'] ?>
    <br><br>
    <label for="MovieDate">Movie Date</label>
    <input type="datetime-local" id='Movie_Date' name='Movie_Date'>
    <?= $locals ['message']['MovieDate'] ?>
    <br><br>
    <input type="submit" value='send!'>
</form>
