<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <?php if($locals['theme'] == 'specialPage') {?>
    <?php } ?>
    <title><?= $locals['PageTitle'] ?? 'Welcome to Cinema!' ?></title>
    <link rel="icon" type="image/png" href="<?= APP_BASE_PATH ?>/assets/images/cinema.png">
    <link rel='stylesheet' href='<?= APP_BASE_PATH ?>/assets/styles/main.css'>
    <link rel='stylesheet' href='<?= APP_BASE_PATH ?>/assets/styles/reset.css'>
</head>
<body>
    <header></header>
    <div id='MainContent'>
    <nav id='sidemenu'></nav>
    <main>
    <h1>Welcome</h1>
    <nav>
        <ul>
            <li><a href='<?= APP_BASE_PATH ?>/'>Home</a></li>
            <li><a href='<?= APP_BASE_PATH ?>/cinema_menu'>Go to Cinema</a></li>
            <li><a href='<?= APP_BASE_PATH ?>/movie_menu'>Go to Movie</a></li>
        </ul>
    </nav>

    <?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>
    </main>
    </div>
    <footer></footer>
</body>
</html>