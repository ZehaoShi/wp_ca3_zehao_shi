<?php return function($req, $res) {

    $db = require('lib/database.php');

    $error_msg = [];
    $Sanitize_msg = [];

    //Validation
    if(empty($req->body('Cinema_id'))){
        $error_msg["EmptyID"] = "You have to enter the Cinema ID" ;
    }
    if(empty($req->body('Cinema_Name'))){
        $error_msg["EmptyName"] = "You have to enter the Cinema Name" ;
    }
    if(empty($req->body('Cinema_Address'))){
        $error_msg["CinemaAddress"] = "You have to enter the Cinema Address" ;
    }
    if(empty($req->body('Land_Line'))){
        $error_msg["LandLine"] = "You have to enter the LandLine" ;
    }
    if(empty($req->body('Open_Time'))){
        $error_msg["OpenTime"] = "You have to enter Open Time" ;
    }
    if(empty($req->body('Closed_Time'))){
        $error_msg["ClosedTime"] = "You have to enter Closed Time" ;
    }
    if(!preg_match('@\d@', $req->body('Cinema_id'), $matches)){
        $error_msg["ErrorID"] = "You only can enter the digits!" ;
    }
    if(!preg_match('@[A-Za-z0-9]@', $req->body('Cinema_Name'), $matches)){
        $error_msg["nameError"] = "You can't enter special character!" ;
    }
    if(!preg_match('@[A-Za-z0-9]@', $req->body('Cinema_Address'), $matches)){
        $error_msg["addressError"] = "You can't enter special character!" ;
    }
    if(preg_match('@[^\d]@', $req->body('Land_Line'), $matches)){
        $error_msg["landlineError"] = "This is not a number!" ;
    }

    //Sanitize
    if (!filter_var($req->body('Cinema_id'), FILTER_VALIDATE_INT )) {
        $Sanitize_msg["NotID"] = "Not ID" ;
    }
    if (!filter_var($req->body('Cinema_Name'), FILTER_SANITIZE_STRING)) {
        $Sanitize_msg["NotName"] = "Name not a String" ;
    }
    if (!filter_var($req->body('Cinema_Address'), FILTER_SANITIZE_STRING)) {
        $Sanitize_msg["NotAddress"] = "Address not a String" ;
    }
    if (!filter_var($req->body('Land_Line'), FILTER_VALIDATE_INT )) {
        $Sanitize_msg["NotLandline"] = "Landline not a Number" ;
    }

    if(empty($error_msg)){
        $CinemaID = $req->body('Cinema_id');
        $cinema_name = $req->body('Cinema_Name');
        $cinema_address = $req->body('Cinema_Address');
        $land_line = $req->body('Land_Line');
        $open_time = $req->body('Open_Time');
        $closed_time = $req->body('Closed_Time');

        $statement = $db->prepare('UPDATE cinemas SET CinemaName = :cinema_name, CinemaAddress = :cinema_address, LandLine = :land_line, OpenTime = :open_time, ClosedTime = :closed_time WHERE CinemaID = :cinema_id');
        $statement->bindValue('cinema_id', $CinemaID);
        $statement->bindValue('cinema_name', $cinema_name);
        $statement->bindValue('cinema_address', $cinema_address);
        $statement->bindValue('land_line', $land_line);
        $statement->bindValue('open_time', $open_time);
        $statement->bindValue('closed_time', $closed_time);
        $statement->execute();
        $cinema = $statement->fetchAll();
        $statement->closeCursor();
        
        $res->redirect("/display_cinema?success=1&Cinema_Name=$cinema_name");
    }

    $res->render('main', 'update_cinema', [
        'message' => $error_msg,
        'sanitize_msg' => $Sanitize_msg
    ]);

} ?>