<?php return function($req, $res) {
    $db = require('lib/database.php');
    $UpdateID = $req->query("Cinema_id");

    //Get cinema value
    $query1 = $db->prepare('SELECT CinemaID, CinemaName, CinemaAddress, LandLine, OpenTime, ClosedTime FROM cinemas WHERE CinemaID = ?');
    $query1->bindParam(1, $UpdateID, PDO::PARAM_INT);
    $query1->execute();

    $cinema = $query1->fetch();
    $query1->closeCursor();


    $res->render('main', 'update_cinema', [
        'success' => $req->query('success') === '1',
        'PageTitle' => 'Update Cinema',
        'updateid' => $UpdateID,
        'cinemaParameter' => $cinema
    ]);


} ?>