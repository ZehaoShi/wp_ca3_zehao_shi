<?php return function($req, $res) {
    $db = require('lib/database.php');

    $error_msg = [];
    $Sanitize_msg = [];
    $cinema = [];

    //Validation
    if(empty($req->body('Cinema_id'))){
        $error_msg["EmptyID"] = "You have to enter the Cinema ID" ;
    }
    if(!preg_match('@\d@', $req->body('Cinema_id'), $matches)){
        $error_msg["ErrorID"] = "You only can enter the digits!" ;
    }

    //Sanitize filter
    if (!filter_var($req->body('Cinema_id'), FILTER_VALIDATE_INT )) {
        $Sanitize_msg["NotID"] = "Not ID" ;
    }
    
    if(empty($error_msg)){
        $CinemaID = $req->body('Cinema_id');

        $query2 = $db->prepare('SELECT CinemaID, CinemaName, CinemaAddress, LandLine, OpenTime, ClosedTime FROM cinemas WHERE CinemaID = :Cinemaid');
        $query2->bindValue('Cinemaid', $CinemaID);
        $query2->execute();
        
        $cinema = $query2->fetchAll();
        $query2->closeCursor();

    }
    
    $res->render('main', 'search_cinema', [
        'message' => $error_msg,
        'sanitize_msg' => $Sanitize_msg,
        'cinemaResult' => $cinema
    ]);


} ?>