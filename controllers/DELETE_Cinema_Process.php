<?php return function($req, $res) {

    $db = require('lib/database.php');

    $error_msg = [];
    $Sanitize_msg = [];

    //Validation
    if(empty($req->body('Cinema_id'))){
        $error_msg["EmptyID"] = "You have to enter the Cinema ID" ;
    }
    if(!preg_match('@\d@', $req->body('Cinema_id'), $matches)){
        $error_msg["ErrorID"] = "You only can enter the digits!" ;
    }

    //Sanitize filter
    if (!filter_var($req->body('Cinema_id'), FILTER_VALIDATE_INT )) {
        $Sanitize_msg["NotID"] = "Not ID" ;
    }

    if(empty($error_msg)){

        $CinemaID = $req->body('Cinema_id');
        
        $query = $db->prepare('DELETE FROM movie WHERE CinemaID = :Cinemaid');
        $query->bindValue('Cinemaid', $CinemaID);
        $query->execute();
        
        $cinema = $query->fetchAll();
        $query->closeCursor();

        $query2 = $db->prepare('DELETE FROM cinemas WHERE CinemaID = :Cinemaid');
        $query2->bindValue('Cinemaid', $CinemaID);
        $query2->execute();
        
        $movie = $query2->fetchAll();
        $query2->closeCursor();
        $res->redirect("/delete_cinemas?success=1");
    }


    $res->render('main', 'delete_cinemas', [
        'message' => $error_msg,
        'sanitize_msg' => $Sanitize_msg
    ]);

} ?>