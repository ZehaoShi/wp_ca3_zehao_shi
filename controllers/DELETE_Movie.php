<?php return function($req, $res) {

$db = require('lib/database.php');
$DeleteID = $req->query("Movie_ID");

$query2 = $db->prepare('DELETE FROM movie WHERE MovieID = :Movieid');
$query2->bindValue('Movieid', $DeleteID);
$query2->execute();

$movie = $query2->fetchAll();
$query2->closeCursor();
$res->redirect("/display_movie?success=1&Delete_MovieID=$DeleteID");

} ?>