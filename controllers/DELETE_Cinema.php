<?php return function($req, $res) {
    
    $db = require('lib/database.php');
    $DeleteID = $req->query("Cinema_id");

    $query = $db->prepare('DELETE FROM movie WHERE CinemaID = :Cinemaid');
    $query->bindValue('Cinemaid', $DeleteID);
    $query->execute();
    
    $cinema = $query->fetchAll();
    $query->closeCursor();

    $query2 = $db->prepare('DELETE FROM cinemas WHERE CinemaID = :Cinemaid');
    $query2->bindValue('Cinemaid', $DeleteID);
    $query2->execute();
    
    $movie = $query2->fetchAll();
    $query2->closeCursor();
    $res->redirect("/display_cinema?success=1&Delete_CinemaID=$DeleteID");

} ?>