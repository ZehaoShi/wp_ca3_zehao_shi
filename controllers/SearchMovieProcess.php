<?php return function($req, $res) {
    $db = require('lib/database.php');

    $error_msg = [];
    $Sanitize_msg = [];
    $movie = [];

    //Validation
    if(empty($req->body('Movie_ID'))){
        $error_msg["EmptyID"] = "You have to enter the Movie ID" ;
    }
    if(!preg_match('@\d@', $req->body('Movie_ID'), $matches)){
        $error_msg["ErrorID"] = "You only can enter the digits!" ;
    }

    //Sanitize filter
    if (!filter_var($req->body('Movie_ID'), FILTER_VALIDATE_INT )) {
        $Sanitize_msg["NotID"] = "Not ID" ;
    }
    
    if(empty($error_msg)){
        $MovieID = $req->body('Movie_ID');

        $query2 = $db->prepare('SELECT MovieID, CinemaID, MovieName, MovieLength, MovieDescription, MovieDate FROM movie WHERE MovieID = :Movieid');
        $query2->bindValue('Movieid', $MovieID);
        $query2->execute();
        
        $movie = $query2->fetchAll();
        $query2->closeCursor();

    }
    
    $res->render('main', 'search_movie', [
        'message' => $error_msg,
        'sanitize_msg' => $Sanitize_msg,
        'MovieResult' => $movie
    ]);


} ?>