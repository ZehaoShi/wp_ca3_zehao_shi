<?php return function($req, $res) {

    $db = require('lib/database.php');

    $query1 = $db->prepare('SELECT CinemaID, CinemaName, CinemaAddress, LandLine, OpenTime, ClosedTime FROM cinemas ORDER BY CinemaID ASC');
    $query1->execute();

    $cinema = $query1->fetchAll();
    $query1->closeCursor();

    $res->render('main', 'display_cinema', [
        'cinemas' => $cinema, 
        'PageTitle' => 'Display Cinema'
    ]);


} ?>