<?php return function($req, $res) {

$db = require('lib/database.php');

$error_msg = [];
$Sanitize_msg = [];

//Validation
if(empty($req->body('Cinema_id'))){
    $error_msg["EmptyCinemaID"] = "You have to enter the Cinema ID" ;
}
if(empty($req->body('Movie_Name'))){
    $error_msg["MovieName"] = "You have to enter the Movie Name" ;
}
if(empty($req->body('Movie_Length'))){
    $error_msg["MovieLength"] = "You have to enter the Movie Length" ;
}
if(empty($req->body('Movie_Description'))){
    $error_msg["MovieDescription"] = "You have to enter Movie Description" ;
}
if(empty($req->body('Movie_Date'))){
    $error_msg["MovieDate"] = "You have to Select Movie Date" ;
}
if(!preg_match('@\d@', $req->body('Cinema_id'), $matches)){
    $error_msg["ErrorID"] = "You only can enter the digits!" ;
}
if(!preg_match('@[A-Za-z0-9]@', $req->body('Movie_Name'), $matches)){
    $error_msg["nameError"] = "You can't enter special character!" ;
}
if(preg_match('@[^\d]@', $req->body('Movie_Length'), $matches)){
    $error_msg["ErrorLength"] = "You only can enter the digits!" ;
}

//Sanitize
if (!filter_var($req->body('Movie_Name'), FILTER_SANITIZE_STRING)) {
    $Sanitize_msg["MovieName"] = "Movie Name not a String" ;
}
if (!filter_var($req->body('Movie_Description'), FILTER_SANITIZE_STRING)) {
    $Sanitize_msg["MovieDescription"] = "Movie Description not a String" ;
}
if (!filter_var($req->body('Movie_Length'), FILTER_VALIDATE_INT )) {
    $Sanitize_msg["MovieLength"] = "Movie Length is not number" ;
}


if(empty($error_msg)){
    $Cinema_id = $req->body('Cinema_id');
    $Movie_Name = $req->body('Movie_Name');
    $Movie_Length = $req->body('Movie_Length');
    $Movie_Description = $req->body('Movie_Description');
    $Movie_Date = $req->body('Movie_Date');

    $statement = $db->prepare('INSERT INTO movie (CinemaID, MovieName, MovieLength, MovieDescription, MovieDate) VALUES(:Cinema_id, :Movie_Name, :Movie_Length, :Movie_Description, :Movie_Date)');
    $statement->bindValue('Cinema_id', $Cinema_id);
    $statement->bindValue('Movie_Name', $Movie_Name);
    $statement->bindValue('Movie_Length', $Movie_Length);
    $statement->bindValue('Movie_Description', $Movie_Description);
    $statement->bindValue('Movie_Date', $Movie_Date);
    $statement->execute();
    $cinema = $statement->fetchAll();
    $statement->closeCursor();


    //Update DB to add the student(example)

    $res->redirect("/add_Movies?success=1&Movie_Name=$Movie_Name");
}

$res->render('main', 'add_Movies', [
    'message' => $error_msg,
    'sanitize_msg' => $Sanitize_msg
]);
} ?>