<?php return function($req, $res) {

    $db = require('lib/database.php');

    $query1 = $db->prepare('SELECT MovieID, CinemaID, MovieName, MovieLength, MovieDescription, MovieDate FROM movie ORDER BY MovieID ASC');

    $query1->execute();

    $movie = $query1->fetchAll();
    $query1->closeCursor();

    $res->render('main', 'display_movie', [
        'movies' => $movie, 
        'PageTitle' => 'Display Movies'
    ]);


} ?>