<?php return function($req, $res) {

    $db = require('lib/database.php');
    $UpdateID = $req->query("Movie_ID");
    $UpdateCinemaID = $req->query("Cinema_id");

    //Get movie value
    $query1 = $db->prepare('SELECT * FROM movie WHERE MovieID = ?');
    $query1->bindParam(1, $UpdateID, PDO::PARAM_INT);
    $query1->execute();

    $movie = $query1->fetch();
    $query1->closeCursor();

    $res->render('main', 'update_movies', [
        'success' => $req->query('success') === '1',
        'PageTitle' => 'Update Movie',
        'updateid' => $UpdateID,
        'updatecinemaid' => $UpdateCinemaID,
        'movieParameter' => $movie
    ]);

} ?>