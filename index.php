<?php
try{
  define('APP_BASE_URL', '');

  $config = require('config.php');
  define('APP_BASE_PATH', $config['app_base_url']);

  // Include the Rapid library
  require_once('lib/Rapid.php');

  // Create a new Router instance
  $app = new \Rapid\Router();

  $app->GET('/','Home'); /* second '' call controller*/
  $app->GET('/display_cinema','DisplayCinema');
  $app->GET('/add_Cinemas','ADD_Cinema');
  $app->GET('/search_cinema','SearchCinema');
  $app->GET('/delete_cinemas','DELETE_Cinema');
  $app->GET('/update_cinema','UPDATE_Cinema');
  $app->GET('/add_Movies','ADD_Movies');
  $app->GET('/update_movies','UPDATE_Movies');
  $app->GET('/search_movie','SearchMovie');
  $app->GET('/display_movie','DisplayMovies');
  $app->GET('/delete_movies','DELETE_Movie');
  $app->GET('/cinema_menu','CinemaMenu');
  $app->GET('/movie_menu','MovieMenu');

  $app->POST('/add_Cinemas','ADDCinemaProcess');
  $app->POST('/search_cinema','SearchCinemaProcess');
  $app->POST('/delete_cinemas','DELETE_Cinema_Process');
  $app->POST('/update_cinema','UPDATE_Cinema_Process');
  $app->POST('/add_Movies','ADDMoviesProcess');
  $app->POST('/update_movies','UPDATE_Movies_Process');
  $app->POST('/search_movie','SearchMovieProcess');
  $app->POST('/delete_movies','DELETE_Movie_Process');

  // Process the request
  $app->dispatch();
}catch(\Rapid\RouteNotFoundException $e){
    $res = $e->getResponseObject();
    $res->status(404);
    $res->render('main', '404', []);
}catch(PDOException $e){
  throw $e;
} 
catch(Exception $e) {
  http_response_code(500);
  exit();
}
?>