<?php 
    class CinemaInfo {
        private $CinemaID;
        private $CinemaName;
        private $CinemaAddress;
        private $LandLine;
        private $OpenTime;
        private $ClosedTime;

        public function __construct($row) {
            $this->CinemaID = $row['CinemaID'];
            $this->CinemaName = $row['CinemaName'];
            $this->CinemaAddress = $row['CinemaAddress'];
            $this->LandLine = $row['LandLine'];
            $this->OpenTime = $row['OpenTime'];
            $this->ClosedTime = $row['ClosedTime'];
        }

        public function DisplayCinema($cinema){
            $db->prepare('SELECT CinemaID, CinemaName, CinemaAddress, LandLine, OpenTime, ClosedTime FROM cinemas ORDER BY CinemaID ASC');
            $db->exec();
        }

        public static function UpdateCinema($update){
            $db->prepare('UPDATE cinemas SET CinemaName = :cinema_name, CinemaAddress = :cinema_address, LandLine = :land_line, OpenTime = :open_time, ClosedTime = :closed_time WHERE CinemaID = :cinema_id');
            $db->exec();

            return new CinemaInfo($res);
        }
    }


?>