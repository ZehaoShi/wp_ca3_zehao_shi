<?php 
    class MovieInfo {
        private $MovieID;
        private $CinemaID;
        private $MovieName;
        private $MovieLength;
        private $MovieDescription;
        private $MovieDate;

        public function __construct($row) {
            $this->MovieID = $row['MovieID'];
            $this->CinemaID = $row['CinemaID'];
            $this->MovieName = $row['MovieName'];
            $this->MovieLength = $row['MovieLength'];
            $this->MovieDescription = $row['MovieDescription'];
            $this->MovieDate = $row['MovieDate'];
        }

        public function DisplayMovie($movie){
            $db->prepare('SELECT MovieID, CinemaID, MovieName, MovieLength, MovieDescription, MovieDate FROM movie ORDER BY MovieID ASC');
            $db->exec();
        }

        public static function UpdateMovie($update){
            $db->prepare('UPDATE movie SET CinemaID = :Cinema_id, MovieName = :Movie_Name, MovieLength = :Movie_Length, MovieDescription = :MovieDescription, MovieDate = :Movie_Date WHERE MovieID = :Movie_ID');
            $db->exec();

            return new MovieInfo($res);
        }
    }


?>