# Cinemas README  

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/ZehaoShi/wp_ca3_zehao_shi/src/master/)  

## Table of Contents  

- [Project Introduction](#markdown-header-project-introduction)
- [Installation](#markdown-header-installation)
- [Website Overview](#markdown-header-website-overview)
- [Reference Link](#markdown-header-reference-link)
- [Video Presentation](#markdown-header-video-presentation)
- [FTP Remote Link](#markdown-header-ftp-remote-link)  

# Project Introduction  
This website allows user to look up cinemas and the movies which are related to the specific cinema. Users are allowed to Add, Update, Delete, Display and Search Cinemas and Movies.  

What the webiste provide for   

- Allow Users to add new cinemas and movies quickly.   

- Any website information can be deleted or updated by users just by clicking the button.  
 
- It might have some bugs which are unfixed, the website is still on testing.   

# Installation  
Simply download the project folder and install to Local HardDisk\xampp\htdocs\ 

![](/ReadMeImage/installation.png)  

##### IMPORTANT   

When importing .sql into database, please BACKUP and DROP the previous database "cinema", CREATE a NEW database named "cinema", then import "cinema.sql" into the database.


# Website Overview  

Here is how the website cinema part looks like, movie part is much similar like that   
![](/ReadMeImage/home.png)
![](/ReadMeImage/Cinema_page.png)
![](/ReadMeImage/add_cinemas.png)
![](/ReadMeImage/display_all_cinemas.png)
![](/ReadMeImage/Search_id.png)
![](/ReadMeImage/update_cinema.png)   


# Reference Link  

Reference Links for projects are below.  

| Reference Content | Reference Link |
| ------ | ------ | 
| Preg Match | http://www.runoob.com/php/php-preg_match.html |   
| Sanitize | https://www.w3schools.com/php/php_filter.asp   |   
| PDO Bind param | http://php.net/manual/zh/pdostatement.bindparam.php  |

# Video Presentation   

This is my video screencast [Screencase Link Here](https://youtu.be/P6kM_Qkbh_8)   

Here are the website links in the video. It doesn't help me, but just in case, i will list these links   

[Die Function](https://www.w3schools.com/php/func_misc_die.asp)   
[editing-form-to-sanitize-validate-phone-number](https://stackoverflow.com/questions/15978667/editing-form-to-sanitize-validate-phone-number/15978779)   

# FTP Remote Link  

[My FTP Remote Link](https://joyous-river.000webhostapp.com/)